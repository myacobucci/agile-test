from bs4 import BeautifulSoup
import codecs
from lxml import html
import re


class HtmlObject(object):
    """ It read an html file from a path and make a soup object. Have methods to process it """

    def __init__(self, path):
        html = open(path).read()
        self.soup = BeautifulSoup(html)

    def get_element_by_id(self, id_element):
        return self.soup.find(id=id_element)


class HtmlInspector(object):
    """ It analyzes a origin html element and finds similar objects in other html """

    def __init__(self, origin_html_element, other_html_file_path):
        self.origin_html_element = origin_html_element
        self.other_html_file_path = other_html_file_path
        other_html = open(other_html_file_path).read()
        self.other_html_file_soup = BeautifulSoup(other_html)

    def find_similar_elements_by_class(self):
        origin_html_classes = self.origin_html_element.attrs['class']
        targets = {}
        for html_class in origin_html_classes:
            candidates = self.other_html_file_soup.find_all("a", class_=html_class)
            # It gives 2 points as is it the most important filter
            self.update_scoring_dict(targets, candidates, 2)
        return targets

    def find_similar_elements_by_text(self):
        origin_html_text = self.origin_html_element.get_text()
        # delete new lines, whitespaces and get the words
        html_text_words = origin_html_text.rstrip().strip().split()
        targets = {}
        for text_word in html_text_words:
            candidates = self.other_html_file_soup.find_all("a", text=re.compile(text_word))
            self.update_scoring_dict(targets, candidates, 1)
        return targets

    @staticmethod
    def update_scoring_dict(targets, candidates, points):
        for candidate in candidates:
            if candidate in targets.keys():
                targets[candidate] += points
            else:
                targets[candidate] = points
        return targets

    def find_most_similar_element(self):
        similar_elements_filters = [self.find_similar_elements_by_class(), self.find_similar_elements_by_text()]
        similar_elements = []
        for similar_filter in similar_elements_filters:
            similar_elements.extend(similar_filter.keys())
        similar_elements = list(set(similar_elements))
        if len(similar_elements) > 0:
            best_candidate_scoring = 0
            best_candidate = ""
            for element in similar_elements:
                scoring = 0
                for similar_filter in similar_elements_filters:
                    if element in similar_filter.keys():
                        scoring += similar_filter[element]
                if scoring > best_candidate_scoring:
                    best_candidate_scoring = scoring
                    best_candidate = element
            return best_candidate
        else:
            raise Exception('Not elements were found')

    def get_xpath_for_most_similar_element(self):
        most_similar_element = self.find_most_similar_element().get_text().rstrip().strip()
        other_html_file = codecs.open(self.other_html_file_path, 'r')
        root = html.fromstring(other_html_file.read())
        tree = root.getroottree()
        sas = root.xpath('//*[contains(text(),"element")]'.replace('element', most_similar_element))[0]
        return tree.getpath(sas)
