Smart XML Analyzer
------------------

This program analyzes an element of an origin Html by an id and find the most similar element on another html file.


Requirements
------------

Please run these commands:

>pip install beautifulsoup4


How to run the program
----------------------

Just run:

>python smart_xml_analyzer.py <input_origin_file_path> <input_other_sample_file_path>

