import sys
from html_inspector import HtmlObject, HtmlInspector
from settings import ID_ELEMENT_TARGET


def xml_path_to_element(origin_file_path, other_file_path):
    origin_html = HtmlObject(origin_file_path)
    origin_html_element = origin_html.get_element_by_id(ID_ELEMENT_TARGET)

    html_inspector = HtmlInspector(origin_html_element, other_file_path)
    print html_inspector.get_xpath_for_most_similar_element()


def main():
    origin_file_path = sys.argv[1]
    other_file_path = sys.argv[2]

    return xml_path_to_element(origin_file_path, other_file_path)


if __name__ == "__main__":
    main()
